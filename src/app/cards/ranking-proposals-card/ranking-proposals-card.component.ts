import { Component } from '@angular/core';
import { RankingService } from '../../services/ranking.service';
import { Proposal } from '../../models/proposal';

@Component({
  selector: 'cards-ranking-proposals',
  templateUrl: './ranking-proposals-card.html',
  styleUrls: ['./ranking-proposals-card.scss'],
  providers: [RankingService],  
})
export class RankingProposalsCardComponent {
  proposals: Proposal[];
  
  constructor( private rankingService: RankingService ) {      
    this.rankingService.proposalsRanking().subscribe(
      proposals => {        
          this.proposals = proposals;
      },
    );   
  }

}
