import { Component } from '@angular/core';
import { ActionService } from '../../services/action.service';
import { Action } from '../../models/action';

@Component({
  selector: 'cards-action',
  templateUrl: './action-card.html',
  styleUrls: ['./action-card.scss'],
  providers: [ActionService],  
})
export class ActionCardComponent {

  actions: Action[];

  constructor( private actionService: ActionService ) {
    this.actionService.list().subscribe(
      actions => {        
          this.actions = actions;
      },
    );    
  }
}
