import { Component } from '@angular/core';
import { RankingService } from '../../services/ranking.service';
import { User } from '../../models/user';

@Component({
  selector: 'cards-ranking-users',
  templateUrl: './ranking-users-card.html',
  styleUrls: ['./ranking-users-card.scss'],
  providers: [RankingService],  
})
export class RankingUsersCardComponent {
  users: User[];
  
  constructor( private rankingService: RankingService ) {      
    this.rankingService.usersRanking().subscribe(
      users => {        
          this.users = users;
      },
    );   
  }

}
