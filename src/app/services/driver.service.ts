import { Injectable } from '@angular/core';
import { Driver } from '../models/driver';
import { Objective } from '../models/objective';
import { Observable } from 'rxjs/Observable';
import { Restangular } from 'ngx-restangular';
import * as _ from 'lodash';

@Injectable()
export class DriverService {

  constructor ( private restangular: Restangular ) {}

  list (objective: Objective, params?: any): Observable<Driver[]> {        
    if (!_.isObject(params)) {
      params = {};
    }

    params['page'] = params['page'] || 1;
    params['per_page'] = params['per_page'] || 5;
    let objective_id = null;
    // if(objective){
    //   objective_id = objective.id;
    // }else{
    //   objective_id = 
    // }
    return this.restangular.one('strategic_objectives', objective.id).all('strategic_drivers').getList();
    
    // return this.restangular.one(objective.id).all('strategic_drivers').getList(params);
  }

}
