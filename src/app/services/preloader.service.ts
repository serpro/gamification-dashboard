import { Injectable } from '@angular/core';

@Injectable()
export class PreloaderService {

  private static _loaders: Promise<any>[] = [];

  static registerLoader(method: Promise<any>): void {
    PreloaderService._loaders.push(method);
  }

  static clear(): void {
    PreloaderService._loaders = [];
  }

  static load(): Promise<any> {
    return new Promise((resolve, reject) => {
      PreloaderService._executeAll(resolve);
    });
  }

  private static _executeAll(done: Function): void {
    setTimeout(() => {
      Promise.all(PreloaderService._loaders).then((values) => {
        done.call(null, values);

      }).catch((error) => {
        console.error(error);
      });
    });
  }
}
