import { Component } from '@angular/core';
import { RankingService } from '../services/ranking.service';
import { Proposal } from '../models/proposal';
import * as _ from 'lodash';

@Component({
  selector: 'ranking-proposals',
  templateUrl: './ranking-proposals.html',
  styleUrls: ['./ranking-proposals.scss'],
  providers: [RankingService],  
})
export class RankingProposalsComponent {
  proposals: Proposal[];
  page = 1;
  
  constructor( private rankingService: RankingService ) {      
     this.loadUsers();
   }

  nextPage(){
    this.page += 1;
    this.loadUsers({page: this.page});
  }
  previousPage(){
    this.page -= 1;
    this.loadUsers({page: this.page});
  }

  isPreviousActive(){
    return this.page > 1;
  }
  isNextActive(){
    return _.size(this.proposals) > 0;
  }

  loadUsers(params = {}){
    const default_params = { 'page': 1, 'per_page': 20 };
    params = Object.assign(default_params, params);
    this.rankingService.proposalsRanking(params).subscribe(
      proposals => {        
          this.proposals = proposals;
      },
    );    
  }

}
