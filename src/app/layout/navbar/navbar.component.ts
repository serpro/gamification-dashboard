import { Component } from '@angular/core';
import { GlobalState } from '../../global.state';

import { User } from '../../models/user';
import { UserService } from '../../services/user.service';


@Component({
  selector: 'layout-navbar',
  templateUrl: './navbar.html',
  styleUrls: ['./navbar.scss'],
})
export class NavbarComponent {

  isScrolled: boolean = false;
  isMenuCollapsed: boolean = false;
  user: User;

  constructor(private _state: GlobalState, private userService: UserService) {
    this._state.subscribe('menu.isCollapsed', (isCollapsed) => {
      this.isMenuCollapsed = isCollapsed;
    });

    this.user = this.userService.getUser();    
    
    this.userService.userChangeEvent.subscribe(user => {
      this.user = user;
    });
  }

  toggleMenu() {
    this.isMenuCollapsed = !this.isMenuCollapsed;
    this._state.notifyDataChanged('menu.isCollapsed', this.isMenuCollapsed);
    return false;
  }

  scrolledChanged(isScrolled) {
    this.isScrolled = isScrolled;
  }
}
