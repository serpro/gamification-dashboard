import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, fakeAsync, tick, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { TranslateModule } from '@ngx-translate/core';
import * as helpers from '../../../spec/helpers';
import { Observable } from 'rxjs/Observable'; 
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { GlobalState } from '../../global.state';
import { UserService } from '../../services/user.service';

import { NavbarComponent } from './navbar.component';

describe('NavbarComponent', () => {

  let component: NavbarComponent;
  let fixture: ComponentFixture<NavbarComponent>;
  const mocks = helpers.getMocks();
  
  beforeEach(async(() => {    
    TestBed.configureTestingModule({
      declarations: [NavbarComponent],
      providers: [{ provide: GlobalState, useValue: mocks.globalState }, 
        { provide: UserService, useValue: mocks.userService },
      ],
      imports: [TranslateModule.forRoot(), RouterTestingModule],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    });
    
    fixture = TestBed.createComponent(NavbarComponent);
    component = fixture.componentInstance;
  }));

  
  it('should display the navbar', () => {
    expect(fixture.debugElement.queryAll(By.css('.page-top')).length).toEqual(1);
  });

});
