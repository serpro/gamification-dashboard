import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, fakeAsync, tick, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { TranslateModule } from '@ngx-translate/core';
import * as helpers from '../../../../spec/helpers';
import { Observable } from 'rxjs/Observable'; 
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';

import { MenuItemComponent } from './menu-item.component';

describe('MenuItemComponent', () => {

  let component: MenuItemComponent;
  let fixture: ComponentFixture<MenuItemComponent>;
  const mocks = helpers.getMocks();
  
  // beforeEach(async(() => {    
  //   TestBed.configureTestingModule({
  //     declarations: [MenuItemComponent],
  //     imports: [TranslateModule.forRoot(), RouterTestingModule],
  //     schemas: [CUSTOM_ELEMENTS_SCHEMA],
  //   });
    
  //   fixture = TestBed.createComponent(MenuItemComponent);
  //   component = fixture.componentInstance;
  // }));

  // it('should display the menu item', () => {
  //   expect(fixture.debugElement.queryAll(By.css('.al-sidebar-list-item')).length).toEqual(1);
  // });

});
