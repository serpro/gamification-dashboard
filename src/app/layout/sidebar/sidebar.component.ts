import { Component, ElementRef, HostListener, OnInit, AfterViewInit } from '@angular/core';
import { GlobalState } from '../../global.state';
import { layoutSizes } from '../layout.constants';
import { User } from '../../models/user';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'layout-sidebar',
  templateUrl: './sidebar.html',
  styleUrls: ['./sidebar.scss'],
})
export class SidebarComponent implements OnInit, AfterViewInit {
  menuHeight: number;
  isMenuCollapsed: boolean = false;
  isMenuShouldCollapsed: boolean = false;
  user: User;
  

  constructor(private _elementRef: ElementRef, private _state: GlobalState, 
    private userService: UserService) {
    this._state.subscribe('menu.isCollapsed', (isCollapsed) => {
      this.isMenuCollapsed = isCollapsed;
    });


    this.userService.userChangeEvent.subscribe(user => {
      this.user = user;
    });
  }

  ngOnInit(): void {
    this.user = this.userService.getUser();    

    if (this._shouldMenuCollapse()) {
      this.menuCollapse();
    }
  }

  ngAfterViewInit() {
    setTimeout(() => this.updateSidebarHeight());
  }

  @HostListener('window:resize')
  onWindowResize() {

    const isMenuShouldCollapsed = this._shouldMenuCollapse();

    if (this.isMenuShouldCollapsed !== isMenuShouldCollapsed) {
      this.menuCollapseStateChange(isMenuShouldCollapsed);
    }
    this.isMenuShouldCollapsed = isMenuShouldCollapsed;
    this.updateSidebarHeight();
  }

  menuExpand() {
    this.menuCollapseStateChange(false);
  }

  menuCollapse() {
    this.menuCollapseStateChange(true);
  }

  menuCollapseStateChange(isCollapsed: boolean) {
    this.isMenuCollapsed = isCollapsed;
    this._state.notifyDataChanged('menu.isCollapsed', this.isMenuCollapsed);
  }

  updateSidebarHeight() {
    // TODO: get rid of magic 84 constant
    this.menuHeight = this._elementRef.nativeElement.childNodes[0].clientHeight - 84;
  }
  
  currentProgress() {
    return ((this.user.points - this.user.level.start_range)/(this.user.level.end_range-this.user.level.start_range)) * 100;
  }
  currentLevel() {
    return this.user.level.position;
  }
  nextLevel() {
    return this.user.level.position + 1;
  }

  private _shouldMenuCollapse(): boolean {
    return window.innerWidth <= layoutSizes.resWidthCollapseSidebar;
  }
}
