import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, fakeAsync, tick, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { TranslateModule } from '@ngx-translate/core';
import * as helpers from '../../../spec/helpers';
import { Observable } from 'rxjs/Observable'; 
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';

import { SidebarComponent } from './sidebar.component';

describe('SidebarComponent', () => {

  let component: SidebarComponent;
  let fixture: ComponentFixture<SidebarComponent>;
  const mocks = helpers.getMocks();
  
  beforeEach(async(() => {    
    TestBed.configureTestingModule({
      declarations: [SidebarComponent],
      imports: [TranslateModule.forRoot(), RouterTestingModule],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    });
    
    fixture = TestBed.createComponent(SidebarComponent);
    component = fixture.componentInstance;
  }));
  // FIXME put this test to works
  // it('should display the navbar', () => {
  //   expect(fixture.debugElement.queryAll(By.css('.al-sidebar')).length).toEqual(1);
  // });

});
