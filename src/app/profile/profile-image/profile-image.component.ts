import { Component, Input, OnInit } from '@angular/core';
import * as _ from 'lodash';

import { User } from '../../models/user';
import { UserService } from '../../services/user.service';


@Component({
  selector: 'profile-image',
  templateUrl: './profile-image.html',
  styleUrls: ['./profile-image.scss'],
})
export class ProfileImageComponent implements OnInit {
  
  @Input() user: User;

  @Input() width: number = 80;
  @Input() height: number = 80;
 
  constructor(private userService: UserService) {
    if(!_.isObject(this.user)){
      this.user = this.userService.getUser();
      this.userService.userChangeEvent.subscribe(user => {
        this.user = user;
      });
    }
  }  

  ngOnInit(): void {
    if(!this.user){
      this.user = this.userService.getUser();
    }    
  }

  profileImageLink(){
    let url = '';
    if(this.user.login){
      url = 'http://voce.serpro/api/v1/profiles/' + this.user.login + '/big?key=identifier';
    }else{
      url = '/assets/img/person.svg';
    }
    // url = '/assets/img/person.svg';
    return url;
  }
  
}
