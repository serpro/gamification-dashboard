import { Component } from '@angular/core';
import { BadgeService } from '../services/badge.service';
import { UserService } from '../services/user.service';
import { User } from '../models/user';
import { Badge } from '../models/badge';
import * as _ from 'lodash';

@Component({
  selector: 'badges',
  templateUrl: './badges.html',
  styleUrls: ['./badges.scss'],
  providers: [BadgeService],  
})
export class BadgesComponent {

  badges: Badge[];
  myBadges: Badge[];
  user: User;
  
  constructor( private badgeService: BadgeService, private userService: UserService ) {
    const params = { 'page': 1, 'per_page': 10 };
    this.badgeService.list(params).subscribe(
      badges => {
          this.badges = badges;
      },
    );
    this.user = this.userService.getUser();    
  }

  hasBadge(badge: Badge): boolean{
    return _.includes(this.user.badge_ids, badge.id);
  }
  groupBadges(){
    return _.chunk(this.badges, 4)
  }


  inactiveImagePath(badge){
    return _.replace(badge.picture, '.png', '_inactive.png');
  }
}
