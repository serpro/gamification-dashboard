// import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
// import { async, fakeAsync, tick, ComponentFixture, TestBed } from '@angular/core/testing';
// import { By } from '@angular/platform-browser';
// import { TranslateModule } from '@ngx-translate/core';
// import * as helpers from '../../spec/helpers';
// import { Observable } from 'rxjs/Observable'; 

// import { BadgesComponent } from './badges.component';
// import { BadgeService } from '../services/badge.service';

// describe('BadgesComponent', () => {

//   let component: BadgesComponent;
//   let fixture: ComponentFixture<BadgesComponent>;
//   const mocks = helpers.getMocks();
// //   const actionService = mocks.actionService;
  
//   beforeEach(async(() => {    
//     TestBed.configureTestingModule({
//       declarations: [BadgesComponent],
//       providers: [{ provide: BadgeService, useValue: mocks.badgeService }],
//       imports: [TranslateModule.forRoot()],
//       schemas: [CUSTOM_ELEMENTS_SCHEMA],
//     }).overrideComponent(BadgesComponent, {
//       set: {
//         providers: [
//           { provide: BadgeService, useValue: mocks.badgeService }],
//     }});

//     fixture = TestBed.createComponent(BadgesComponent);
//     component = fixture.componentInstance;
//   }));

//   it('should display the list of badges', () => {
//     expect(fixture.debugElement.queryAll(By.css('.badges-list')).length).toEqual(1);
//   });

// });
