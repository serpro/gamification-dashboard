# Personal Dashboard

[![build status](https://git.serpro/gamification/personal-dashboard/badges/master/build.svg)](https://git.serpro/gamification/personal-dashboard/commits/master)
[![coverage report](https://git.serpro/gamification/personal-dashboard/badges/master/coverage.svg)](https://git.serpro/gamification/personal-dashboard/commits/master)

### Demo

<a target="_blank" href="http://meuserpro.dev.sdr.serpro/">Meu Serpro Demo</a>

## Features
* LDAP Login

