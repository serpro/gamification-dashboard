# config valid only for current version of Capistrano
lock "3.9.0"

#set :application, "personal-dashboard"
#set :repo_url, "git@git.serpro:gamification/personal-dashboard.git"

set :application, "personal-dashboard"
set :base_url, 'https://git.serpro'
set :application, 'personal-dashboard'
set :repo_url, "#{fetch(:base_url)}/gamification/#{fetch(:application)}.git"
set :deploy_to, "/opt/#{fetch(:application)}/"
set :keep_releases, 10


# Default branch is :master
# ask :branch, `git rev-parse --abbrev-ref HEAD`.chomp

# Default deploy_to directory is /var/www/my_app_name
# set :deploy_to, "/var/www/my_app_name"

# Default value for :format is :airbrussh.
# set :format, :airbrussh

# You can configure the Airbrussh format using :format_options.
# These are the defaults.
# set :format_options, command_output: true, log_file: "log/capistrano.log", color: :auto, truncate: :auto

# Default value for :pty is false
# set :pty, true

set :linked_dirs, %w{node_modules}

# Default value for :linked_files is []
# append :linked_files, "config/database.yml", "config/secrets.yml"

# Default value for linked_dirs is []
# append :linked_dirs, "log", "tmp/pids", "tmp/cache", "tmp/sockets", "public/system"

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

# Default value for local_user is ENV['USER']
# set :local_user, -> { `git config user.name`.chomp }

# Default value for keep_releases is 5
# set :keep_releases, 5
desc "Install Nede Modules"
task :npm_install do 
  on roles(:app), in: :sequence, wait: 5 do
    within release_path do 
      execute :npm, "install"
    end
  end
end

after "deploy:published", :npm_install
desc "Make the project build"
task :build do 
  on roles(:app), in: :sequence, wait: 5 do 
    within release_path do 
      execute :ng, "build"
#      execute :ng, "build --prod --env=prod"
    end
  end
end
after :npm_install, :build

#set :default_env, { 
#  path: ["/home/ubuntu/.nvm/versions/node/v6.9.4/bin",
##    "#{shared_path}/node_modules/bower/bin", 
##    "#{shared_path}/node_modules/grunt-cli/bin",
##    "/usr/local/rbenv/versions/#{fetch(:rbenv_ruby)}/bin",
#    "$PATH"].join(":")
#}
